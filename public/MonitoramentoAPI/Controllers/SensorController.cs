﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Monitoramento.API.Services;
using Monitoramento.API.Model;

namespace Monitoramento.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorController : ControllerBase
    {
        public readonly SensorService _sensorService;
        public int QuantidadeBuild { get; set; }
        public SensorController(SensorService SensorService)
        {
            _sensorService = SensorService;
        }

        // GET: api/sensor
        [HttpGet]
        public IEnumerable<Sensor> GetAll()
        {
            return _sensorService.GetSensores();
        }

        // GET: api/sensor/5
        [HttpGet("{id}", Name = "Get")]
        public Sensor GetById(int id)
        {
            return _sensorService.GetSensor(id);
        }

    
    }
}
