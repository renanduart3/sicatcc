﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoramento.API.Model
{
    public class Sensor
    {
        public int Id { get; set; }
        public Ativo Ativo { get; set; }
        public int AtivoId { get; set; }
        public double RegUmidade { get; set; }
        public double RegTemperatura { get; set; }
        public DateTime DataRegistro { get; set; }
    }
}
