﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoramento.API.Model
{
    public class Ativo
    {
        public int AtivoId { get; set; }
        public string Nome { get; set; }
        public string NumeroSerie { get; set; }
        public Local Local { get; set; }
        public int LocalId { get; set; }
    }
}
