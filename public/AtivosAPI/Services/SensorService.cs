﻿using Microsoft.EntityFrameworkCore;
using Monitoramento.API.Data;
using Monitoramento.API.Model;
using Monitoramento.API.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoramento.API.Services
{
    public class SensorService
    {
        private readonly SicaContext _context;

        public SensorService(SicaContext context)
        {
            _context = context;
        }

        public Sensor GetSensor(int id)
        {

            return _context.Monitores.Include(obj => obj.Ativo).FirstOrDefault(s => s.Id == id);

        }

        public IEnumerable<Sensor> GetSensores()
        {
            return _context.Monitores.OrderBy(x => x.Id).ToList();
        }

       

    }
}
