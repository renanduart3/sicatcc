﻿using Microsoft.EntityFrameworkCore;
using Monitoramento.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoramento.API.Data
{
    public class SicaContext:DbContext
    {
        public SicaContext(DbContextOptions<SicaContext> options):base(options)
        {

        }

        public DbSet<Ativo> Ativos { get; set; }
        public DbSet<Local> Locals { get; set; }
        public DbSet<Sensor> Monitores { get; set; }
    }
}
