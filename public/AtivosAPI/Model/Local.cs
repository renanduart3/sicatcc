﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoramento.API.Model
{
    public class Local
    {
        public int LocalId { get; set; }
        public string Nome { get; set; }
    }
}
