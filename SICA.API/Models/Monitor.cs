namespace SICA.API.Models
{
    public class Monitor
    {
        public int MonitorId { get; set; }
        public Ativo Ativo { get; set; }
       public int AtivoId { get; set; }
       public double RegUmidade { get; set; }
        public double RegTemperatura { get; set; }
        
    }
}