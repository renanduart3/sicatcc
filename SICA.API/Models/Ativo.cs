namespace SICA.API.Models
{
    public class Ativo
    {
        public int AtivoId { get; set; }
        public string AtivoNome { get; set; }
        public string NumeroSerie { get; set; }
        public Local Local { get; set; }
        public int LocalId { get; set; }
    }
}