namespace SICA.API.Models
{
    public class Compliance
    {
        public int ComplianceId { get; set; }
        public int IdNorma { get; set; }
        public string DescNorma { get; set; }
    }
}