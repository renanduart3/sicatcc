using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SICA.API.Models;
using SICA.API.Repository;

namespace SICA.API.Controllers
{ 
    [Route("api/[controller]")]
    [ApiController]
    public class MonitorsController:ControllerBase
    {
        private readonly IMonitorRepository _context;

        public MonitorsController(IMonitorRepository context)
        {
            _context = context ??
            throw new ArgumentNullException (nameof(context));
        }

        [HttpGet]
        public IEnumerable<Monitor> GetAll()
        {
            return _context.GetAll();
        }

        [HttpGet("{id}",Name="Find")]
        public IActionResult GetById(int id)
        {
            var monitor = _context.Find(id);
            if( monitor == null)
                return NotFound();
            return new ObjectResult(monitor);
        }

    }
}
