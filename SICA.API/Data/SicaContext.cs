using Microsoft.EntityFrameworkCore;
using SICA.API.Models;

namespace SICA.API.Data
{
    public class SicaContext:DbContext
    {
        public SicaContext(DbContextOptions<SicaContext> options)
            : base(options)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Compliance>()
                .HasIndex(a => a.IdNorma)
                .IsUnique();
        }      
        public DbSet<Ativo> Ativos{get;set;}
        public DbSet<Local> Locals{get;set;}
        public DbSet<Monitor> Monitors{get;set;}
        public DbSet<Compliance> Compliances {get;set;}
    }
}