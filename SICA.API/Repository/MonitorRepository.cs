using System;
using System.Collections.Generic;
using System.Linq;
using SICA.API.Data;
using SICA.API.Models;

namespace SICA.API.Repository
{
    public class MonitorRepository : IMonitorRepository
    {
        public readonly SicaContext _context;

        public MonitorRepository(SicaContext context)
        {
            _context = context ??
                throw new ArgumentNullException (nameof (context));
        }
        public Monitor Find(int id)
        {
            return _context.Monitors.FirstOrDefault(x => x.MonitorId.Equals(id));
        }

        public IEnumerable<Monitor> GetAll()
        {
            return _context.Monitors.ToList();
        }
    }
}