using System.Collections.Generic;
using SICA.API.Models;

namespace SICA.API.Repository
{
    public interface IMonitorRepository
    {
         IEnumerable<Monitor> GetAll();
         Monitor Find(int id);

    }
}